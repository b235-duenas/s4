const express = require("express");
const app = express();
const port = 5001;

app.use(express.json());

let userRoutes = require("./app/routes");
app.use("/users", userRoutes);

app.listen(port, () => console.log(`listening on port: ${port}`));
